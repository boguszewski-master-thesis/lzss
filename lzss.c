
/*
 * lz77.c
 *
 *  Created on: 11 Nov 2021
 *      Author: robsk
 */

/************************************************************************
 * 							INCLUDES
 * **********************************************************************/
#include "lzss.h"

#include <stdlib.h>
#include <string.h>

#include "circular_buffer.h"

/************************************************************************
 * 							PRIVATE DEFINES
 * **********************************************************************/
#define WORD_LEGTH ( (1<<LZSS_WORD_BITS) - 1)

/************************************************************************
 * 							MODULE STRUCT
 * **********************************************************************/
struct lzss{
	cbuf_handle_t 		dicionary;
	cbuf_handle_t 		input;

    lzss_compressed_t   out_compressed;
    lzss_no_compress_t  out_no_compressed;

	int 				elements_left;		// elements left in input buffer
};

/************************************************************************
 * 							PRIVATE MACROS
 * **********************************************************************/
/**
 * 	@brief Return value how many elements left to analyzed
 *	@param han handler to lzss object
 */
#define ELEMENTS_LEFT_TO_ANALYZED(han) (han->elements_left)

/************************************************************************
 * 						PRIVATE FUNCTIONS PROTOTYPE
 * **********************************************************************/
static size_t 	add_new_elements(	lzss_handle_t lzss_han, uint8_t *data, 	size_t count);
static void 	count_elements_left(lzss_handle_t lzss_han, size_t initial, size_t analyzed);
static void 	find_repetition(	lzss_handle_t lzss_han);
static void 	move_byte_from_intput_to_dic(lzss_handle_t lzss_han);

/************************************************************************
 * 							PUBLIC FUNCTIONS
 * **********************************************************************/
/**
 * 	@brief 	Create LZSS handler, manage memory for this
 *	@return handler to lzss object
 */
lzss_handle_t lzss_create()
{
    uint16_t dicionary_size = 1;

    lzss_handle_t lzss_han = malloc(sizeof(struct lzss));

    // fast "2^(LZSS_OFFSET_BITS) - 1":
    dicionary_size <<= LZSS_OFFSET_BITS;
    dicionary_size --;

    lzss_han->dicionary = c_buffer_create(dicionary_size);
    lzss_han->input     = c_buffer_create(LZSS_INPUT_BUF_SIZE);

    memset(&lzss_han->out_no_compressed, 0, sizeof(lzss_han->out_no_compressed));

    return lzss_han;   
}

/**
 * 	@brief 	Destroy handler for LZSS algorithm, release resources
 *	@param 	lzss_han 		handler to lzss object
 */
void lzss_destroy(lzss_handle_t lzss_han)
{
    c_buffer_destroy(lzss_han->dicionary);
	c_buffer_destroy(lzss_han->input);
	free(lzss_han);
}

/**
 * 	@brief 	Compress data using LZSS algorithm
 *	@param 	lzss_han 		handler to lzss object
 *	@param 	data_in			input data
 *	@param 	element_count 	count input elements
 *	@param 	data_out		output buffer
 *	@return size of output buffer
 */
size_t lzss_compress(lzss_handle_t lzss_han, uint8_t *data_in, size_t element_count, uint8_t *data_out)
{
	size_t output_count = 0;	// count elements in output buffer
	int i;

    // new size
    lzss_han->elements_left = add_new_elements(lzss_han, data_in, element_count);
	
	while (lzss_han->elements_left > 0)
	{
		find_repetition(lzss_han);

		// End compression, wait for the next data
		if(lzss_han->out_compressed.matched_legth + 1 >= lzss_han->elements_left) break;

		// if token is smaller than word
		if(lzss_han->out_compressed.matched_legth >= sizeof(lzss_compressed_t))
		{
			lzss_han->out_compressed.occurs		= OCCURS;

			memcpy(data_out, &lzss_han->out_compressed, sizeof(lzss_compressed_t));
			data_out 		+= sizeof(lzss_compressed_t);
			output_count 	+= sizeof(lzss_compressed_t);

			for(i=0; i<lzss_han->out_compressed.matched_legth; i++)
			{
				move_byte_from_intput_to_dic(lzss_han);
				lzss_han->elements_left--;
			}
		}
		else
		{
			for(i=0; i< lzss_han->out_compressed.matched_legth +1; i++)
			{
				lzss_han->out_no_compressed.next_byte = c_buffer_get(lzss_han->input, i);
				memcpy(data_out, &lzss_han->out_no_compressed, sizeof(lzss_no_compress_t));
				data_out 		+= sizeof(lzss_no_compress_t);
				output_count 	+= sizeof(lzss_no_compress_t);
				
				move_byte_from_intput_to_dic(lzss_han);
				lzss_han->elements_left--;
			}
		}		
	}
	
	return output_count;
}


/************************************************************************
 * 							PRIVATE FUNCTIONS
 * **********************************************************************/
/**
 * 	@brief move input data into circular buffer
 *	@param lzss_han handler to lzss object
 *	@param data_in	input data
 *	@param count 	how many elements put into buffer
 */
static size_t add_new_elements(lzss_handle_t lzss_han, uint8_t *data_in, size_t count)
{
	uint32_t i;

	//add new data chuck
    for(i=0; i<count; i++)
	{
		c_buffer_add(lzss_han->input, data_in[i]);
	}

	return c_buffer_count(lzss_han->input);
}

/**
 * 	@brief count how many elements left to analaze
 *	@param lzss_han handler to lzss object
 *	@param initial	how much elements want to analyzed when function start
 *	@param analyzed how many elements was already analyzed
 */
inline static void count_elements_left(lzss_handle_t lzss_han, size_t initial, size_t analyzed)
{
	lzss_han->elements_left = initial - analyzed;
}

/**
 * 	@brief Find next repetition in dicionary
 *	@param lzss_han handler to lzss object
 */
static void find_repetition(lzss_handle_t lzss_han)
{
	uint32_t i,j;
	size_t count_element_in_dic;
	uint8_t value_dic, value_in;

	count_element_in_dic = c_buffer_count(lzss_han->dicionary);
	memset(&lzss_han->out_compressed, 0, sizeof(lzss_han->out_compressed));

	for(i = 0; i<count_element_in_dic; i++)
	{
		for(j=0; j<(WORD_LEGTH) && i >= j && j < lzss_han->elements_left; j++)
		{
			value_dic 	= c_buffer_get_back(lzss_han->dicionary, i - j);
			value_in 	= c_buffer_get(lzss_han->input, j);

			if(value_dic != value_in)	break;
		}

		//Get the longest or last matched is eaqual with previous one 
		if(j >= lzss_han->out_compressed.matched_legth && j > 0)
		{
			lzss_han->out_compressed.matched_legth 		= j;
			lzss_han->out_compressed.matched_position 	= i + 1;
		}

		if(lzss_han->out_compressed.matched_legth == WORD_LEGTH) break;
	}
}

/**
 * 	@brief Move one byte of data from input buffer into output dicionary
 *	@param lzss_han handler to lzss object
 */
inline static void move_byte_from_intput_to_dic(lzss_handle_t lzss_han)
{
	c_buffer_add(lzss_han->dicionary, c_buffer_pop(lzss_han->input));
}