/*
 * lz77.h
 *
 *  We asume tha we working only for asci sign
 */

#ifndef __LZSS_H__
#define __LZSS_H__

#include <stdint.h>
#include <stddef.h>

#define LZSS_OCCURS_BIT     1
#define LZSS_WORD_BITS      5
#define LZSS_OFFSET_BITS    (16 - LZSS_WORD_BITS - LZSS_OCCURS_BIT)

#define LZSS_INPUT_BUF_SIZE 400


typedef struct lzss * lzss_handle_t;

typedef enum lzss_no_occurs_flag{
    NO_OCCURS,
    OCCURS
}lzss_no_occurs_flag_t;

/*
    2 bytes struct
*/
typedef struct lzss_compressed{
    lzss_no_occurs_flag_t	occurs 	        :LZSS_OCCURS_BIT;
	uint16_t	            matched_legth 	:LZSS_WORD_BITS;
	uint16_t	            matched_position:LZSS_OFFSET_BITS;

} __attribute__((packed)) lzss_compressed_t;

/*
    Only 1 byte struct
*/
typedef struct lzss_no_compress
{
    lzss_no_occurs_flag_t   occurs      : LZSS_OCCURS_BIT;  // flag to indicate that 
    uint8_t                 next_byte   : (8 - LZSS_OCCURS_BIT);
} __attribute__((packed)) lzss_no_compress_t;

lzss_handle_t   lzss_create();
void            lzss_destroy(lzss_handle_t lzss_han);

size_t lzss_compress(lzss_handle_t lzss_han, uint8_t *data_in, size_t element_count, uint8_t *data_out);

#endif /* __LZSS_H__ */
