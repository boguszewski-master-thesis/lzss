cmake_minimum_required(VERSION 3.14)

set(CIRCULAR_BUFFER_TEST OFF)

project(lzss_test)

# GoogleTest requires at least C++11
set(CMAKE_CXX_STANDARD 11)

add_executable(
    ${PROJECT_NAME}
    lzss.c
)

target_include_directories(
    ${PROJECT_NAME}
    PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}
)

target_link_libraries(
    ${PROJECT_NAME}
    PUBLIC
    circular_buffer
    gtest_main
)

if(NOT TARGET circular_buffer)
    add_subdirectory(circular_buffer)
endif()

add_subdirectory(google)
add_subdirectory(tests)

include(GoogleTest)

gtest_discover_tests(${PROJECT_NAME})