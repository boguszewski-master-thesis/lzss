cmake_minimum_required(VERSION 3.14)

enable_testing()

target_sources(
  ${PROJECT_NAME}
  PRIVATE
  ${CMAKE_CURRENT_SOURCE_DIR}/lzss.cpp
)