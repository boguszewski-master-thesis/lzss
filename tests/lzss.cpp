#include <gtest/gtest.h>
#include <stdlib.h>
#include <string.h>

extern "C" {
  #include "lzss.h"
}

uint8_t raw[] = "LOREM LOREM LOREM LOREM LOREM ";
lzss_no_compress_t excepted_out[] ={
  {.next_byte = 'L'},
  {.next_byte = 'O'},
  {.next_byte = 'R'},
  {.next_byte = 'E'},
  {.next_byte = 'M'},
  {.next_byte = ' '}
};
/*
{
  (uint8_t *)((lzss_no_compress_t){ 0, 'L'}),
  (lzss_no_compress_t){ 0, 'O'}),
  (lzss_no_compress_t){ 0, 'R'}),
  (lzss_no_compress_t){ 0, 'E'}),
  (lzss_no_compress_t){ 0, 'M'}),
  (lzss_no_compress_t){ 0, ' '}),
  (lzss_compressed_t){ 0, 6, 6, 'L'}),
};
*/

TEST(LZSS, HelloWorld)
{
  EXPECT_EQ(2+2, 4);
}

TEST(LZSS, Compress_RAW_test)
{
  lzss_handle_t lzss_han;
  lzss_no_compress_t out[100];
  size_t legth = 0;
  size_t count_compressed;

  lzss_han = lzss_create();

  count_compressed =  lzss_compress(lzss_han, raw, sizeof(raw) - 1, (uint8_t *)&out[0]);
  EXPECT_EQ(memcmp(out, excepted_out, sizeof(excepted_out) ), 0);
}